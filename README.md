# Working with keystone_clidev

## Pre-requisites

### Make sure versions in both the host and the remote guest match
This is especially important for both tomcat as well as the JDK, because if you
do not, you may experience random failures which I did not have the time to
pinpoint.

### Make sure that your tomcat installation is clean of old applications
Just to be sure to debug the proper code, it may be advisable that you first
clean all the apps in your tomcat server prior to start a debugging session.
You can achieve that by doing:

```sh
sudo rm /opt/tomcat/webapps/*.war
```

### Install idea, if it is not installed

Idea is needed to edit files, and some of the examples assume that Idea is
installed either in the host or the VM, especially for VMs like keystone_xfce or
keystone_dev or keystone_lxde.

Additionally, you will need to do a few additional setups on your host, depending
on whether you are using Windows or OSX.

### Install fuse in OSX (optional)
This portion is optional, but makes it easier for cases where you define
remote servers in Intellij

This is needed when you are [defining an application server](https://www.jetbrains.com/idea/help/defining-application-servers-in-intellij-idea.html) in Intellij.

Intellij assumes needs to know, how the remote tomcat look. There are two ways
to achieve this. One is to give Intellij a direct view into the remote filesystem
where tomcat is. Another is to provide a local version of tomcat which is exactly
like the one on the remote end. Fuse uses the former strategy, providing you
direct access to the VM filesystem.


In order to be able to access the remote fileserver of keystone_clidev you need to install several components:

```
brew cask install osxfuse
brew install sshfs
```
> Note: The brew cask install is needed because OS-X Yosemite restrictions.

After you installed sshfs, you need to mount the root filesystem of
your VM into the host.

```
mkdir tmp
sshfs vagrant@42.40.40.42:/ ./tmp
```

## Guest IP addresses that you will be using

Most of the examples use http://localhost:8080, however that will work only if
you are querying things from within the VM or alternately, you have a
Virtualbox based VM and you have forwarded port 8080 into the host.

There are however a couple of alternate ways to get to the web service layer on
the guest VM.

* If the VM you are using is running in vSphere, then use
  ```vagrant ssh-config``` to obtain the ip address of your VM.

* If you are running your VMs in virtualbox, then you may access the VM
  via a private host interface. The following table lists the addresses for the
  different VMs:

VM type          |  Address from host
-----------------|-------------------
keystone_clidev  | 42.40.40.42
keystone_dev     | 42.40.40.40
keystone_lxde    | 42.40.40.40
keystone_xfce    | 42.40.40.40

So, in the following examples, just remember to replace http://localhost:8080
by whichever address you have your guest at.

## Intellij tasks common to all projects
For all projects, you will need to import a gradle project from intellij,
making sure to use the default gradle wrapper. After the Intellij project
creation completes, make sure to check that the JDK for the project corresponds
to the JDK being used in the remote end. You can check this by looking in
File / Project Structure / Project and check for the SDK value.

## Remote debug hello world

Go to the folder hello-remote-debug and compile the hello world application

```sh
gradle clean build
```

Now, run the resulting executable with java

```sh
java -cp build/libs/hello-remote-debug-1.0-SNAPSHOT.jar com.example.App
```

The application will run until you enter 'q' as shown below:

```sh
$ java -cp build/libs/hello-remote-debug-1.0-SNAPSHOT.jar com.example.App
Hello and welcome to the World of remote debugging
Enter something --> Hello
Hello
Enter something --> q
q
$
```

Now in order to debug the application, we need to pass the following additional
arguments to java:
```sh
-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n
```
or alternately:

```sh
-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=5005,suspend=n
```

> Note: The difference between the two option sets is that the first one works
  from Java 5.0 - also named 1.5 on. The second one works in Java 1.4, but is
  also supported in newer versions of Java.

The invocation command line then becomes:

```sh
java -agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n \
     -cp build/libs/hello-remote-debug-1.0-SNAPSHOT.jar com.example.App
```


When invoked in this way, the application will start and listen to debugger
commands on port 5005.

## Debug a JAX-RS web-service in standalone HttpServer

For this example, you will first compile and run a service and then you will debug the
service.

### Compile and run the service

Go to the folder folder simple-service, which is a basic JAX-RS
application from [Jersey 2.4 user guide](https://jersey.java.net/documentation/latest/user-guide.html),
which I ported to use gradle and added a few goodies to make the application
easily debuggable in Intellij

```sh
cd simple-service
```

Build and run the application

```sh
gradle clean run
```

> Note: Make sure that Tomcat or any service using the 8080 port is stopped prior to running the
  above command, otherwise you will get a failure.

Upon starting you should see:
```
$ gradle clean run
:clean
:compileJava
Note: /home/vagrant/Documents/debugging-with-idea/simple-service/src/main/java/com/example/Main.java uses or overrides a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
:processResources UP-TO-DATE
:classes
:run
Sep 19, 2016 6:18:34 PM org.glassfish.grizzly.http.server.NetworkListener start
INFO: Started listener bound to [0.0.0.0:8080]
Sep 19, 2016 6:18:34 PM org.glassfish.grizzly.http.server.HttpServer start
INFO: [HttpServer] Started.
Jersey app started with WADL available at http://0.0.0.0:8080/myapp/application.wadl
Hit enter to stop it...
> Building 80% > :run
```

The gradle task will stop at that point waiting for you to hit enter.

You can check that the service is running by issuing the following command from
within the VM:

```sh
curl http://localhost:8080/myapp/application.wadl
```

The above command will return the WADL for your service. The command output
should be similar to the following:

```sh
$ curl  http://localhost:8080/myapp/application.wadl
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<application xmlns="http://wadl.dev.java.net/2009/02">
    <doc xmlns:jersey="http://jersey.java.net/" jersey:generatedBy="Jersey: 2.14 2014-12-11 07:22:06"/>
    <doc xmlns:jersey="http://jersey.java.net/"
         jersey:hint="This is simplified WADL with user and core resources only. To get full WADL with extended resources use the query parameter detail. Link: http://localhost:8080/myapp/application.wadl?detail=true"/>
    <grammars/>
    <resources base="http://localhost:8080/myapp/">
        <resource path="myresource">
            <method id="getIt" name="GET">
                <response>
                    <representation mediaType="text/plain"/>
                </response>
            </method>
        </resource>
    </resources>
</application>
$
```

In order to get the the resource type:

```sh
curl  http://localhost:8080/myapp/myresource
```

The output for the above command should be ```Got It!```

Hit enter in the console where you issue ```gradle run``` to terminate the
application.

### Debug the service

In order to debug the service you need to issue the same gradle run command,
but this time you should pass the argument `--debug-jvm`. The command line
to issue is then:

```sh
gradle clean run --debug-jvm
```

Running the above command produces the following output:

```sh
$ gradle clean run --debug-jvm
:clean
:compileJava
Note: /vm_repo/scratchpad/debugging-with-idea/simple-service/src/main/java/com/example/Main.java uses or overrides a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
:processResources UP-TO-DATE
:classes
:run
Listening for transport dt_socket at address: 5005
> Building 80% > :run
```

Notice above the address at which the JVM is waiting for a debugger connection
which is 5005. In order to debug the service you need then to create an Intellij
*remote* configuration which points at port 5005.

### Debug the service in a docker container

Debugging things in a docker container is equivalent to debugging the service in a
remote host. The following paragraphs show how you should get things started
to debug the service running in a container.

#### Start your container
You need a container which has the right java version and the gradle
tools. After executing the following command you will be left inside a container
that has java and gradle

```bash
docker run --name=tester --rm -v $PWD:$PWD  -ti isreg/is-tester:0.1.0 bash       
Unable to find image 'isreg/is-tester:0.1.0' locally
0.1.0: Pulling from isreg/is-tester
...
...
Status: Downloaded newer image for isreg/is-tester:0.1.0
root@63e2454d699f:/
# 18:51:45 > 
```

### Start the service as described in debug the service

First you need to go to the folder where your sources are located:

```sh
cd /home/vagrant/Documents/debugging-with-idea/simple-service/
```

> Note: The exact path will depend on where your $PWD matches when starting 
  the container. In my case, it was the path above, so adjust as needed.
  
Now start the service

```sh
# 18:58:29 > gradle clean run --debug-jvm
:clean
:compileJava
...
...
Note: Recompile with -Xlint:deprecation for details.
:processResources UP-TO-DATE
:classes
:run
Listening for transport dt_socket at address: 5005
> Building 80% > :run

```

### Pre-check things
Once you started the service using the gradle command line above, you should be
able to connect to it via an Intellij remote debug configuration, however, keep in
mind that gradle is at this point waiting for a debugger to connect to port 5005
and as such, port 8080 which is the service port will not be available. You can
verify this by checking the ports on the container. In order to do this, we first
need to login to the container via a secondary console:

```sh
docker exec -ti tester bash
root@63e2454d699f:/
# 19:04:32 > netstat -ln
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 0.0.0.0:5005            0.0.0.0:*               LISTEN     
udp6       0      0 :::50642                :::*                               
Active UNIX domain sockets (only servers)
Proto RefCnt Flags       Type       State         I-Node   Path
root@63e2454d699f:/
# 19:04:40 > 

```

As can be seen above, address 5005 which is the remote jdwp port is open for connections.
As soon as you connect via Intellij, you will notice that port 5005 will stop listening for connections
on the local container stack and port 8080 will start listening. In order to setup your Intellij
*remote* connection, you need to obtain the ip address of your container. For this, just issue the
 command `ip a` from the second container console you just started:
 
```sh
# 19:09:09 > ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
30: eth0@if31: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:2/64 scope link 
       valid_lft forever preferred_lft forever
root@63e2454d699f:/
# 19:09:11 > 
```

In the above case, the interface address you want is eth0@if31, which has ip address 172.17.0.2.

> Note: The interface name as well as the IP address may vary for you. What you need to make sure
  in general is that the docker interface you use is not the `lo` interface.

Once you start your Intellij remote configuration to point to the container address using port 5005,
you should see the following ports listening on the container:

```sh
# 19:09:11 > netstat -ln
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp6       0      0 :::8080                 :::*                    LISTEN     
udp6       0      0 :::50642                :::*                               
Active UNIX domain sockets (only servers)
Proto RefCnt Flags       Type       State         I-Node   Path
root@63e2454d699f:/

```

As you can see above, port 5005 is no longer listening because IntelliJ is debugging the service.
You can also note that the actual service port 8080 is listening which is what we want.

You can now test the service by issuing the service call via curl to the container from the host:

```sh
$ curl 172.17.0.2:8080/myapp/myresource
Got it!      
```


## Debug a basic JEE project

Go to the folder folder simple-service-webapp, which is a basic JAX-RS JEE
application from [Jersey 2.4 user guide](https://jersey.java.net/documentation/latest/user-guide.html),
which I ported to use gradle.

```sh
cd simple-service-webapp
```

Build the application

```sh
gradle build
```

The application war file will be built into the build/libs and conforms to the
gradle application conventions, whereby the war version is included in the
file name. The application can be deployed to tomcat, by simply copying the war
file to the tomcat webapp folder.

```
sudo cp build/libs/simple-service-webapp-1.0-SNAPSHOT.war /opt/tomcat/webapps
```

The service wadl can be consulted issuing from a browser

```
http://localhost:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/application.wadl
```

or using curl

```
curl http://localhost:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/application.wadl
```


The wadl in either case returns:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<application xmlns="http://wadl.dev.java.net/2009/02">
    <doc xmlns:jersey="http://jersey.java.net/" jersey:generatedBy="Jersey: 2.14 2014-12-11 07:22:06"/>
    <doc xmlns:jersey="http://jersey.java.net/" jersey:hint="This is simplified WADL with user and core resources only. To get full WADL with extended resources use the query parameter detail. Link: http://localhost:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/application.wadl?detail=true"/>
    <grammars/>
    <resources base="http://localhost:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/">
        <resource path="myresource">
            <method id="getIt" name="GET">
                <response>
                    <representation mediaType="text/plain"/>
                </response>
            </method>
        </resource>
    </resources>
</application>
```

The service can be queried using
```
http://localhost:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/myresource
```
and returns
```
Got it!
```

Once we have the service running, we can debug it with Intellij.

### Start the remote TOMCAT in debug mode
There are a variety of ways that you can start tomcat in debug mode, so I will
tackle just one. The simplest way to start tomcat in debug mode is to stop the
service and then restart it by calling the tomcat startup program directly
passing in the required options. The following command sequence which has to
be run from within the VM does just that

```sh
sudo service apache-tomcat-8.0.15 stop
cd /opt/tomcat/bin
sudo -u tomcat JPDA_ADDRESS="33131" ./catalina.sh jpda start
```

The above command will start tomcat at the normal 8080 address but will use
as debugging port 33131. You can verify that tomcat is running by issuing
the following command:

```sh
ps aux | grep tomcat
```

Your output should be similar to the following one:

```sh
vagrant@ubuntu-14:/opt/tomcat/bin$ ps aux | grep tomcat
tomcat   18938  7.7 11.9 2943692 244324 pts/4  Sl   22:08   0:12 /usr/bin/java
  -Djava.util.logging.config.file=/opt/apache-tomcat-8.0.15/conf/logging.properties
  -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
  -agentlib:jdwp=transport=dt_socket,address=33131,server=y,suspend=n
  -Djava.endorsed.dirs=/opt/apache-tomcat-8.0.15/endorsed
  -classpath /opt/apache-tomcat-8.0.15/bin/bootstrap.jar:/opt/apache-tomcat-8.0.15/bin/tomcat-juli.jar
  -Dcatalina.base=/opt/apache-tomcat-8.0.15
  -Dcatalina.home=/opt/apache-tomcat-8.0.15
  -Djava.io.tmpdir=/opt/apache-tomcat-8.0.15/temp
  org.apache.catalina.startup.Bootstrap start
```

The important part is this line:

`
-agentlib:jdwp=transport=dt_socket,address=33131,server=y,suspend=n
`

This states that tomcat started in debug mode using **jdwp** socket transport over
port **33131**. This is the port that any remote debugger will need to use to
communicate with this tomcat instance.

### Creating a remote debug configuration in Intellij

* Select drop-down menu **Run / Edit Configurations...**
* Click on the '+' button in the upper left corner
* In the "Add New Configuration" drop-down select **Remote**
* In the Name text box type something that helps you identify this debug
  configuration for later.
* In the configuration tab on the right, look for the section named host and
  enter the address of your host. Refer to the Table above in section
  **Guest IP addresses that you will be using**
* In the port tab, enter the port address which you used to **Start the remote
  tomcat in debug mode**.

Once you created the debug configuration, you can test it by selecting your
newly created debug configuration, from the top right and then clicking the bug
right next to it.

Assuming you used host=42.40.40.42 and port= 33131 when creating your
configuration and that tomcat is running in keystone_clidev, you should get a
message similar to this one:

```
Connected to the target VM, address: '42.40.40.42:33131', transport: 'socket'
```

### Test things out

On your host, edit the file  simple-service-webapp/src/main/java/com/example/MyResource.java.

Go to the area that has the REST response for GET

```java
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
```

Insert a breakpoint in the line that reads ```return "Got it!"```.

Now open a browser on your host and navigate to the following url:

```
http://42.40.40.42:8080/simple-service-webapp-1.0-SNAPSHOT/webapi/myresource
```

At this point, Intellij, should stop at the breakpoint you just defined.

References:
----------

* [How to remotely debug application running on tomcat from within intellij idea](http://blog.trifork.com/2014/07/14/how-to-remotely-debug-application-running-on-tomcat-from-within-intellij-idea)
* [Java Platform Debugger Architecture](https://docs.oracle.com/javase/8/docs/technotes/guides/jpda/jpda.html)
* [JPDA connection and invocation details](http://docs.oracle.com/javase/7/docs/technotes/guides/jpda/conninv.html)
