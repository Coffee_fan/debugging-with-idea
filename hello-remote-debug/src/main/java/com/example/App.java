package com.example;

import java.util.Scanner;

/**
 * Hello world for remote debugging.
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "\nHello and welcome to the World of remote debugging" );
        Scanner scanner = new Scanner(System.in);
        String input;
        do {
            System.out.print("Enter something --> ");
            input = scanner.nextLine();
            System.out.println(input);
        } while( input.compareTo("q") != 0);
    }
}
